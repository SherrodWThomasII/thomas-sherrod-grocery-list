﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThomasSherrod_GroceryList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        GroceryData GD
        {
            get
            { // Make the foodSupplies from the GroceryData = the HaveBtn
                GroceryData returnGrocery = new GroceryData();
                returnGrocery.foodSupplies = txtFoodSupplies.Text;
                return returnGrocery; // returns the grocery list
            }
        }

        private void HaveBtn_Click(object sender, EventArgs e)
        {
            // Adds whatever the user entered into the Have List
            HaveList.Items.Add(GD); // Adds the items to the Have list
            txtFoodSupplies.Clear(); // Clears the text in food/supplies

        }

        private void NeedBtn_Click(object sender, EventArgs e)
        {
            // Adds whatever the user entered into the Need List
            NeedList.Items.Add(GD); // Adds the items to the Need list
            txtFoodSupplies.Clear(); // Clears the text in food/supplies
        }

        private void HavetoNeedbtn_Click(object sender, EventArgs e)
        {
            //Switches the have list item to the need list item
            if(HaveList.SelectedItem != null)
            {
                NeedList.Items.Add((GroceryData)HaveList.SelectedItem); 
                HaveList.Items.Remove((GroceryData)HaveList.SelectedItem); // The add and remove gives the illusion of moving an item.
            }
        }

        private void NeedtoHavebtn_Click(object sender, EventArgs e)
        {
            //Switches the need list item to the have list item
            if(NeedList.SelectedItem != null)
            {
                HaveList.Items.Add((GroceryData)NeedList.SelectedItem);
                NeedList.Items.Remove((GroceryData)NeedList.SelectedItem);
            }
        }

        private void HaveRemovebtn_Click(object sender, EventArgs e)
        {
            if(HaveList.SelectedItem != null)
            {
                HaveList.Items.Remove((GroceryData)HaveList.SelectedItem); // Removes item from Have List
            }
        }

        private void NeedRemovebtn_Click(object sender, EventArgs e)
        {
            if(NeedList.SelectedItem != null)
            {
                NeedList.Items.Remove((GroceryData)NeedList.SelectedItem); // Removes item from Need List
            }
        }
    }
}
