﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThomasSherrod_GroceryList
{
    class GroceryData
    {
        // public food/supply
        public string foodSupplies;
        // What will show in the columns/listboxes
        public override string ToString()
        {
            return foodSupplies; // returns the foodSupplies from the public string
        }
    }
}
