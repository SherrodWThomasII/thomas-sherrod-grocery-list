﻿namespace ThomasSherrod_GroceryList
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HaveList = new System.Windows.Forms.ListBox();
            this.HaveGroup = new System.Windows.Forms.GroupBox();
            this.Needgroup = new System.Windows.Forms.GroupBox();
            this.NeedList = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFoodSupplies = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.HaveBtn = new System.Windows.Forms.Button();
            this.NeedBtn = new System.Windows.Forms.Button();
            this.HavetoNeedbtn = new System.Windows.Forms.Button();
            this.NeedtoHavebtn = new System.Windows.Forms.Button();
            this.HaveRemovebtn = new System.Windows.Forms.Button();
            this.NeedRemovebtn = new System.Windows.Forms.Button();
            this.HaveGroup.SuspendLayout();
            this.Needgroup.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // HaveList
            // 
            this.HaveList.FormattingEnabled = true;
            this.HaveList.ItemHeight = 25;
            this.HaveList.Location = new System.Drawing.Point(18, 68);
            this.HaveList.Name = "HaveList";
            this.HaveList.Size = new System.Drawing.Size(334, 254);
            this.HaveList.TabIndex = 0;
            // 
            // HaveGroup
            // 
            this.HaveGroup.Controls.Add(this.HaveList);
            this.HaveGroup.Location = new System.Drawing.Point(437, 120);
            this.HaveGroup.Name = "HaveGroup";
            this.HaveGroup.Size = new System.Drawing.Size(376, 345);
            this.HaveGroup.TabIndex = 2;
            this.HaveGroup.TabStop = false;
            this.HaveGroup.Text = "Have";
            // 
            // Needgroup
            // 
            this.Needgroup.Controls.Add(this.NeedList);
            this.Needgroup.Location = new System.Drawing.Point(931, 120);
            this.Needgroup.Name = "Needgroup";
            this.Needgroup.Size = new System.Drawing.Size(357, 345);
            this.Needgroup.TabIndex = 3;
            this.Needgroup.TabStop = false;
            this.Needgroup.Text = "Need";
            // 
            // NeedList
            // 
            this.NeedList.FormattingEnabled = true;
            this.NeedList.ItemHeight = 25;
            this.NeedList.Location = new System.Drawing.Point(18, 68);
            this.NeedList.Name = "NeedList";
            this.NeedList.Size = new System.Drawing.Size(324, 254);
            this.NeedList.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NeedBtn);
            this.groupBox1.Controls.Add(this.HaveBtn);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtFoodSupplies);
            this.groupBox1.Location = new System.Drawing.Point(12, 154);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(356, 192);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Grocery Items";
            // 
            // txtFoodSupplies
            // 
            this.txtFoodSupplies.Location = new System.Drawing.Point(183, 52);
            this.txtFoodSupplies.Name = "txtFoodSupplies";
            this.txtFoodSupplies.Size = new System.Drawing.Size(154, 31);
            this.txtFoodSupplies.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Food/Supplies";
            // 
            // HaveBtn
            // 
            this.HaveBtn.Location = new System.Drawing.Point(11, 116);
            this.HaveBtn.Name = "HaveBtn";
            this.HaveBtn.Size = new System.Drawing.Size(131, 52);
            this.HaveBtn.TabIndex = 2;
            this.HaveBtn.Text = "Have";
            this.HaveBtn.UseVisualStyleBackColor = true;
            this.HaveBtn.Click += new System.EventHandler(this.HaveBtn_Click);
            // 
            // NeedBtn
            // 
            this.NeedBtn.Location = new System.Drawing.Point(183, 116);
            this.NeedBtn.Name = "NeedBtn";
            this.NeedBtn.Size = new System.Drawing.Size(131, 52);
            this.NeedBtn.TabIndex = 3;
            this.NeedBtn.Text = "Need";
            this.NeedBtn.UseVisualStyleBackColor = true;
            this.NeedBtn.Click += new System.EventHandler(this.NeedBtn_Click);
            // 
            // HavetoNeedbtn
            // 
            this.HavetoNeedbtn.Location = new System.Drawing.Point(828, 206);
            this.HavetoNeedbtn.Name = "HavetoNeedbtn";
            this.HavetoNeedbtn.Size = new System.Drawing.Size(85, 52);
            this.HavetoNeedbtn.TabIndex = 4;
            this.HavetoNeedbtn.Text = "-->";
            this.HavetoNeedbtn.UseVisualStyleBackColor = true;
            // 
            // NeedtoHavebtn
            // 
            this.NeedtoHavebtn.Location = new System.Drawing.Point(828, 325);
            this.NeedtoHavebtn.Name = "NeedtoHavebtn";
            this.NeedtoHavebtn.Size = new System.Drawing.Size(85, 52);
            this.NeedtoHavebtn.TabIndex = 5;
            this.NeedtoHavebtn.Text = "<--";
            this.NeedtoHavebtn.UseVisualStyleBackColor = true;
            // 
            // HaveRemovebtn
            // 
            this.HaveRemovebtn.Location = new System.Drawing.Point(561, 485);
            this.HaveRemovebtn.Name = "HaveRemovebtn";
            this.HaveRemovebtn.Size = new System.Drawing.Size(131, 52);
            this.HaveRemovebtn.TabIndex = 4;
            this.HaveRemovebtn.Text = "Remove";
            this.HaveRemovebtn.UseVisualStyleBackColor = true;
            // 
            // NeedRemovebtn
            // 
            this.NeedRemovebtn.Location = new System.Drawing.Point(1041, 485);
            this.NeedRemovebtn.Name = "NeedRemovebtn";
            this.NeedRemovebtn.Size = new System.Drawing.Size(131, 52);
            this.NeedRemovebtn.TabIndex = 6;
            this.NeedRemovebtn.Text = "Remove";
            this.NeedRemovebtn.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1340, 571);
            this.Controls.Add(this.NeedRemovebtn);
            this.Controls.Add(this.HaveRemovebtn);
            this.Controls.Add(this.NeedtoHavebtn);
            this.Controls.Add(this.HavetoNeedbtn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Needgroup);
            this.Controls.Add(this.HaveGroup);
            this.Name = "Form1";
            this.Text = "Grocery List";
            this.HaveGroup.ResumeLayout(false);
            this.Needgroup.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox HaveList;
        private System.Windows.Forms.GroupBox HaveGroup;
        private System.Windows.Forms.GroupBox Needgroup;
        private System.Windows.Forms.ListBox NeedList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button NeedBtn;
        private System.Windows.Forms.Button HaveBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFoodSupplies;
        private System.Windows.Forms.Button HavetoNeedbtn;
        private System.Windows.Forms.Button NeedtoHavebtn;
        private System.Windows.Forms.Button HaveRemovebtn;
        private System.Windows.Forms.Button NeedRemovebtn;
    }
}

