﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThomasSherrod_GroceryList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        GroceryData GD
        {
            get
            { // Make the foodSupplies from the GroceryData = the HaveBtn
                GroceryData returnGrocery = new GroceryData();
                returnGrocery.foodSupplies = txtFoodSupplies.Text;
                return returnGrocery; // returns the grocery list
            }
        }

        private void HaveBtn_Click(object sender, EventArgs e)
        {
            // Adds whatever the user entered into the Have List
            HaveList.Items.Add(GD); // Adds the items to the Have list
            txtFoodSupplies.Clear(); // Clears the text in food/supplies

        }

        private void NeedBtn_Click(object sender, EventArgs e)
        {
            // Adds whatever the user entered into the Need List
            NeedList.Items.Add(GD); // Adds the items to the Need list
            txtFoodSupplies.Clear(); // Clears the text in food/supplies
        }
    }
}
